﻿using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.File;
using System;

namespace AutoPause.Helpers
{
    /// <summary>
    /// Helper class for logging messages to the console and file.
    /// </summary>
    public static class ConsoleLogger
    {
        static readonly object _writeLock = new object();

        /// <summary>
        /// Writes a message to the console with the specified color and logs it to the file.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="color">The color of the console output.</param>
        public static void WriteLine(object message, ConsoleColor color = ConsoleColor.White)
        {
            lock (_writeLock)
            {
                Console.ForegroundColor = color;
                Console.WriteLine($"[{DateTime.Now:HH:mm:ss.fff}] {message}");
                Console.ResetColor();
            }

            Log.Information("{Message}", message); // Logging to the file AutoPause.log
        }

        /// <summary>
        /// Builds a logger instance for logging to the console and file.
        /// </summary>
        /// <param name="sourceContext">The context/source of the logging.</param>
        /// <returns>An instance of ILogger.</returns>
        internal static Microsoft.Extensions.Logging.ILogger BuildLogger(string sourceContext = null)
        {
            // Serilog configuration for logging to console and file
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss.fff}] [{Level:u4}] " + (sourceContext ?? "{SourceContext}") + ": {Message:lj}{NewLine}{Exception}")
                .WriteTo.File("AutoPause.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            // Creating and returning an instance of ILogger
            return new LoggerFactory().AddSerilog().CreateLogger(string.Empty);
        }
    }
}
