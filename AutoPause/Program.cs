﻿using AutoPause.Helpers;
using AutoPause.Managers;
using System;
using System.Collections.Generic;

namespace AutoPause
{
    /// <summary>
    /// Entry point for the AutoPause application.
    /// </summary>
    class Program
    {
        private static MultimediaManager multimediaManager;
        private static Configuration config;
        private static LockUnlockManager lockUnlockManager;

        /// <summary>
        /// Main method to start the AutoPause application.
        /// </summary>
        /// <param name="args">Command-line arguments.</param>
        static void Main(string[] args)
        {
            ConsoleLogger.WriteLine("== Starting AutoPause application...");

            // Create an instance of the configuration and load parameters
            config = new Configuration();
            Dictionary<string, string> configLoaded = config.LoadConfig();

            // Create and configure the media manager
            multimediaManager = new MultimediaManager();
            multimediaManager.Start();

            // Create and configure the lock/unlock manager
            string lockAction = configLoaded["lock"];
            string unLockAction = configLoaded["unlock"];
            lockUnlockManager = new LockUnlockManager(multimediaManager);
            lockUnlockManager.Start(lockAction, unLockAction);

            ConsoleLogger.WriteLine("== AutoPause application started. Press any key to exit...");
            Console.ReadLine();

            // Stop the media manager and save the configuration before exiting
            config.SaveConfig();
            multimediaManager.Dispose();
        }
    }
}
