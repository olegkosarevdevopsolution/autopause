﻿using AutoPause.Helpers;
using Microsoft.Win32;
using System;
using System.Diagnostics;

namespace AutoPause.Managers
{
    /// <summary>
    /// Manages the locking and unlocking events of the computer and handles multimedia playback accordingly.
    /// </summary>
    public class LockUnlockManager
    {
        /// <summary>
        /// Event triggered when the computer is locked.
        /// </summary>
        public event Action OnLock;

        /// <summary>
        /// Event triggered when the computer is unlocked.
        /// </summary>
        public event Action OnUnlock;

        private MultimediaManager multimediaManager;
        private bool wasPlayingBeforeLock = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="LockUnlockManager"/> class.
        /// </summary>
        /// <param name="MultimediaManager">The multimedia manager instance to control playback.</param>
        public LockUnlockManager(MultimediaManager MultimediaManager)
        {
            multimediaManager = MultimediaManager;
            // Subscribe to the computer lock and unlock events
            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;
        }

        /// <summary>
        /// Starts the LockUnlockManager with the specified lock and unlock actions.
        /// </summary>
        /// <param name="lockAction">The action to perform when the computer is locked.</param>
        /// <param name="unLockAction">The action to perform when the computer is unlocked.</param>
        public void Start(string lockAction, string unLockAction)
        {
            if (lockAction != "none")
            {
                if (lockAction == "pause" || lockAction == "stop")
                {
                    OnLock += HandleLockEvent;
                    ConsoleLogger.WriteLine($"Lock events are enabled in the configuration. Action {lockAction}", ConsoleColor.Yellow);
                }
                else
                {
                    ConsoleLogger.WriteLine($"Lock events are enabled in the configuration. Unknown Action {lockAction}", ConsoleColor.Yellow);

                }
            }
            else
            {
                ConsoleLogger.WriteLine("Lock events are disabled in the configuration.", ConsoleColor.Yellow);
            }

            if (unLockAction != "none")
            {
                if (unLockAction == "play")
                {
                    OnUnlock += HandleUnlockEvent;
                    ConsoleLogger.WriteLine($"UnLock events are enabled in the configuration. Action {unLockAction}", ConsoleColor.Yellow);
                }
                else
                {
                    ConsoleLogger.WriteLine($"UnLock events are enabled in the configuration. Unknown Action {lockAction}", ConsoleColor.Yellow);
                }
            }
            else
            {
                ConsoleLogger.WriteLine("UnLock events are disabled in the configuration.", ConsoleColor.Yellow);
            }

            ConsoleLogger.WriteLine("Lock And Unlock Manager Loaded", ConsoleColor.Yellow);
        }

        private void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            switch (e.Reason)
            {
                case SessionSwitchReason.SessionLock:
                    // Execute actions when the computer is locked
                    OnLock?.Invoke();
                    break;
                case SessionSwitchReason.SessionUnlock:
                    // Execute actions when the computer is unlocked
                    OnUnlock?.Invoke();
                    break;
            }
        }

        private async void HandleLockEvent()
        {
            try
            {
                // Actions to perform when the computer is locked
                ConsoleLogger.WriteLine("Computer locked!", ConsoleColor.Yellow);

                string playbackStatus = multimediaManager.GetPlaybackStatus();
                wasPlayingBeforeLock = (playbackStatus == "Playing");

                if (wasPlayingBeforeLock)
                {
                    await multimediaManager.PauseAsync();
                }
            }
            catch (Exception ex)
            {
                // Handle errors when pausing media playback
                ConsoleLogger.WriteLine($"Error pausing media playback: {ex.Message}", ConsoleColor.Red);
            }
        }

        private async void HandleUnlockEvent()
        {
            // Actions to perform when the computer is unlocked
            ConsoleLogger.WriteLine("Computer unlocked!", ConsoleColor.Yellow);

            try
            {
                // If multimedia was active before locking, resume playback
                if (wasPlayingBeforeLock)
                {
                    await multimediaManager.PlayAsync();
                }
            }
            catch (Exception ex)
            {          
                // Handle errors when resuming media playback
                ConsoleLogger.WriteLine($"Error resuming media playback: {ex.Message}", ConsoleColor.Red);
            }
        }
    }
}
