﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AutoPause.Helpers
{
    /// <summary>
    /// Helper class for loading, saving, and managing configuration settings.
    /// </summary>
    public class Configuration
    {
        private string configFile = "config.json";

        /// <summary>
        /// Loads the configuration settings from the config.json file.
        /// </summary>
        /// <returns>A dictionary containing the loaded configuration settings.</returns>
        public Dictionary<string, string> LoadConfig()
        {
            Dictionary<string, string> defaultConfig = new Dictionary<string, string>
            {
                {"lock", "pause"},
                {"unlock", "play"},
                {"idle", "pause"}
            };

            if (File.Exists(configFile))
            {
                ConsoleLogger.WriteLine($"Config file '{configFile}' found.", ConsoleColor.Yellow);
                try
                {
                    ConsoleLogger.WriteLine("Reading config file.", ConsoleColor.Yellow);
                    // Reading configuration from the file
                    string json = File.ReadAllText(configFile);
                    var config = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                    ConsoleLogger.WriteLine("Config Load successfully.", ConsoleColor.Green);
                    return config ?? defaultConfig;
                }
                catch (Exception ex)
                {
                    // Handling errors while reading the configuration file
                    ConsoleLogger.WriteLine($"Error reading config file: {ex.Message}", ConsoleColor.Red);
                    return defaultConfig;
                }
            }
            else
            {
                // Returning the default value if the configuration file is not found
                ConsoleLogger.WriteLine($"Config file '{configFile}' not found. Using default configuration.", ConsoleColor.Yellow);
                ConsoleLogger.WriteLine("Config Default Load successfully.", ConsoleColor.Green);
                SaveConfig(defaultConfig);
                return defaultConfig;
            }

        }

        /// <summary>
        /// Saves the provided configuration settings to the config.json file.
        /// </summary>
        /// <param name="config">The configuration settings to be saved.</param>
        internal void SaveConfig(Dictionary<string, string> config)
        {
            try
            {
                // Converting configuration to JSON and writing it to the file
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(config, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(configFile, json);
                ConsoleLogger.WriteLine("Config saved successfully.", ConsoleColor.Green);
            }
            catch (Exception ex)
            {
                // Handling errors while saving the configuration file
                ConsoleLogger.WriteLine($"Error saving config file: {ex.Message}", ConsoleColor.Red);
            }
        }

        /// <summary>
        /// Saves the current configuration settings to the config.json file.
        /// </summary>
        internal void SaveConfig()
        {
            ConsoleLogger.WriteLine("Config saved successfully.", ConsoleColor.Green);
        }
    }
}
