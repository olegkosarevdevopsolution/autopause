﻿using AutoPause.Helpers;
using System;
using System.Threading.Tasks;
using WindowsMediaController;

namespace AutoPause.Managers
{
    /// <summary>
    /// Manages multimedia playback control and monitoring.
    /// </summary>
    public class MultimediaManager
    {
        private MediaManager mediaManager;
        static MediaManager.MediaSession currentSession;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultimediaManager"/> class.
        /// </summary>
        public MultimediaManager()
        {
            // Creating and configuring the media manager
            mediaManager = new MediaManager()
            {
                Logger = ConsoleLogger.BuildLogger("MediaManager"),
            };
        }

        /// <summary>
        /// Gets the current media session.
        /// </summary>
        /// <returns>The current media session.</returns>
        public MediaManager.MediaSession GetCurrentSession()
        {
            if (currentSession == null)
            {
                return null;
            }
            return currentSession;
        }

        /// <summary>
        /// Starts the multimedia manager.
        /// </summary>
        public void Start()
        {
            mediaManager.OnFocusedSessionChanged += MediaManager_OnFocusedSessionChanged;
            mediaManager.Start();
            currentSession = mediaManager.GetFocusedSession();
        }

        private static void MediaManager_OnFocusedSessionChanged(MediaManager.MediaSession mediaSession)
        {
            currentSession = mediaSession;
            ConsoleLogger.WriteLine("== Session Focus Changed: " + mediaSession?.ControlSession?.SourceAppUserModelId, ConsoleColor.DarkRed);
        }

        /// <summary>
        /// Asynchronously plays the media.
        /// </summary>
        /// <returns>A task representing the asynchronous operation.</returns>
        public async Task PlayAsync()
        {
            if (GetPlaybackStatus() == "Paused")
            {
                bool setPlay = await currentSession.ControlSession.TryPlayAsync();
                if (setPlay == true)
                {
                    ConsoleLogger.WriteLine($"Current Session {currentSession.Id}. Media playback resumed.", ConsoleColor.Green);
                }
                else
                {
                    ConsoleLogger.WriteLine($"Current Session {currentSession.Id}. Unable to resume media playback.", ConsoleColor.Red);
                }
            }
        }

        /// <summary>
        /// Asynchronously pauses the media.
        /// </summary>
        /// <returns>A task representing the asynchronous operation.</returns>
        public async Task PauseAsync()
        {
            if (GetPlaybackStatus() == "Playing")
            {
                bool setPause = await currentSession.ControlSession.TryPauseAsync();
                if (setPause == true)
                {
                    ConsoleLogger.WriteLine($"Current Session {currentSession.Id}. Media playback paused.", ConsoleColor.Green);
                }
                else
                {
                    ConsoleLogger.WriteLine($"Current Session {currentSession.Id}. Unable to pause media playback.", ConsoleColor.Red);
                }
            }
        }

        /// <summary>
        /// Gets the playback status of the current media session.
        /// </summary>
        /// <returns>The playback status as a string.</returns>
        public string GetPlaybackStatus()
        {
            return currentSession.ControlSession.GetPlaybackInfo().PlaybackStatus.ToString();
        }

        /// <summary>
        /// Disposes of the multimedia manager.
        /// </summary>
        public void Dispose()
        {
            mediaManager.Dispose();
            ConsoleLogger.WriteLine("== AutoPause application stopped.", ConsoleColor.Yellow);
        }
    }
}
